/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/
var  score,roundScore,currentGame,activeplayer, currentPlay, prevDice;
/*roundScore = 0;
score =[0,0];
activeplayer=0;*/


//////////roll dice event listener

document.querySelector('.btn-roll').addEventListener('click',function() {
if (currentPlay){
var dice = Math.floor((Math.random() * 6) + 1);
var dice2 = Math.floor((Math.random() * 6) + 1);

    

 
var diceDom = document.querySelector('#dice2');
diceDom.style.display = 'block';
diceDom.src = 'dice-'+dice2+'.png';     

var diceDom = document.querySelector('#dice1');
diceDom.style.display = 'block';
diceDom.src = 'dice-'+dice+'.png'; 
    


if((dice !== 1 && dice2 !==1) && (dice + prevDice )!== 12 ){
	roundScore+=dice + dice2;
	document.getElementById('current-'+activeplayer).textContent = roundScore;
    prevDice = dice;
}
    else {
       prevDice = 0;    
	   nextPlayer();
}
}});

////////hold event listener

document.querySelector('.btn-hold').addEventListener('click',function(){
    if(currentPlay){
        var inputScore = document.querySelector('.final-score').value;
        score[activeplayer]+=roundScore;
        var winningScore;
        if (inputScore){
            winningScore = inputScore;
        }
        else{
            winningScore = 100;
        }
        document.getElementById('score-'+activeplayer).textContent = score[activeplayer];
        if (score[activeplayer] > winningScore){
            document.getElementById('name-'+activeplayer).textContent = 'WINNER!';
            document.getElementById('dice1').style.display = 'none';
            document.getElementById('dice2').style.display = 'none';
            document.querySelector('.player-'+activeplayer+'-panel').classList.add('winner');
            document.querySelector('.player-'+activeplayer+'-panel').classList.remove('active');
            currentPlay = false;
                                     }    
        else { nextPlayer(); 
             prevDice = 0;}
    
                   }
})



document.querySelector('.btn-new').addEventListener('click',init);

function nextPlayer(){
    activeplayer === 0 ? activeplayer = 1 : activeplayer = 0 ;
    roundScore=0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
        
}
	function init(){
        prevDice = 0;
        roundScore = 0;
        score =[0,0];
        activeplayer=0;
        currentPlay = true;
        document.getElementById('dice1').style.display = 'none';
        document.getElementById('dice2').style.display = 'none';
        document.getElementById('score-0').textContent = 0;
        document.getElementById('score-1').textContent = 0;
        document.getElementById('current-0').textContent = 0;
        document.getElementById('current-1').textContent = 0;
        document.getElementById('name-0').textContent = 'Player 1';
        document.getElementById('name-1').textContent = 'Player 2';
        document.querySelector('.player-0-panel').classList.remove('active');
        document.querySelector('.player-1-panel').classList.remove('active');
        document.querySelector('.player-0-panel').classList.remove('winner');
        document.querySelector('.player-1-panel').classList.remove('winner');
        document.querySelector('.player-0-panel').classList.add('active');
    }												 
													 
													 
													 
													 
													 
													 
													 
												